const vector = [1, 'dos', true, 4.5, false, 'seis'];

// a) Usando for
console.log('Usando for:');
for (let i = 0; i < vector.length; i++) {
  console.log(vector[i]);
}

// b) Usando forEach
console.log('Usando forEach:');
vector.forEach(function (elemento) {
  console.log(elemento);
});

// c) Usando map
console.log('Usando map:');
vector.map(function (elemento) {
  console.log(elemento);
});

// d) Usando while
console.log('Usando while:');
let i = 0;
while (i < vector.length) {
  console.log(vector[i]);
  i++;
}

// e) Usando for..of
console.log('Usando for..of:');
for (const elemento of vector) {
  console.log(elemento);
}